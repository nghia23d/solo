<?php

namespace App\Models;

use App\Models\AdminModel;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Category extends AdminModel
{
    protected $fillable = [
        'id',
        'name',
        'status',
        'is_home',
        'display',
        'created_by',
        'modified_by'
    ];
    public function __construct()
    {
        $this->table        = 'category';
        $this->folderUpload = 'category';
        $this->searchField  = ['id','name'];
        $this->fieldsBlock  = ['_token'];
    }
    public function articles()
    {
        return $this->hasMany('App\Models\Article');
    }

    public function listItems($params = null, $option = null)
    {
        $result = null;
        if ($option['task'] == "admin-list-items") {
            $query = self::select('*')->orderBy('id', 'DESC')->withCount('articles');

            if ($params['filter']['status'] != 'all') {
                $query->where('status', $params['filter']['status']);
            }

            if ($params['search']['value'] !== '') {
                if ($params['search']['field'] == 'all') {
                    $query->where(function ($query) use ($params) {
                        foreach ($this->searchField as $field) {
                            $query->orWhere($field, 'LIKE', "%{$params['search']['value']}%");
                        }
                    });
                } elseif (in_array($params['search']['field'], $this->searchField)) {
                    $query->where($params['search']['field'], 'LIKE', "%{$params['search']['value']}%");
                };
            }

            $result = $query->paginate($params['pagination']['itemsPerPage']);
        }
        if ($option['task'] == "news-list-items") {
            $query  = self::select('id', 'name')
                            ->orderBy('id', 'DESC')
                            ->where('status', 'active');
            $result = $query->get();
        }
        if ($option['task'] == "news-list-items-is-home") {
            $query = self::select('id', 'name', 'display')
                            ->orderBy('id', 'DESC')
                            ->where('status', 'active')
                            ->where('is_home', '1');
            $result = $query->get();
        }
        if ($option['task'] == "admin-list-items-in-selectbox") {
            $query = self::select('id', 'name')
                            ->orderBy('name', 'asc')
                            ->where('status', 'active');
            $result = $query->get();
        }
        return $result;
    }

    public function countItems($params = null, $option = null)
    {
        $result = null;
        if ($option['task'] == "admin-count-items") {
            $query = $this::select('status', DB::raw('count(*) as count, status'));
            $result = $query->groupBy('status')
                            ->get()
                            ->toArray();
        }
        return $result;
    }
    public function saveItem($params = null, $option = null)
    {
        if ($option['task'] == "change-status-item") {
            $revertStatus = [
                'active'   => 'inactive',
                'inactive' => 'active'
            ];
            self::where('id', $params['id'])
                ->update(['status' => $revertStatus[$params['currentStatus']]]);
        }
        if ($option['task'] == "change-is-home") {
            $revertIsHome = [1,0];
            self::where('id', $params['id'])
                ->update(['is_home' => $revertIsHome[$params['currentIsHome']]]);
        }
        if ($option['task'] == "add-item") {
            $params['created_by'] = 'nghia le';
            $params['created']    = now();
            self::insert($this->deleteFieldsBlock($params));
        }
        if ($option['task'] == "edit-item") {
            $params['modified_by'] = 'nghia le';
            self::where('id', $params['id'])->update($this->deleteFieldsBlock($params));
        }
    }
    public function deleteItem($params = null, $option = null)
    {
        if ($option['task'] == "delete-item") {
            $item   = $this->getItem(['id' => $params['id']], ['task' => 'get-item']);
            $item->delete();
        }
    }
    public function getItem($params = null, $option = null)
    {
        $result = null;
        if ($option['task'] == "get-item") {
            $result  = self::find($params['id']);
        }
        return $result;
    }
}
