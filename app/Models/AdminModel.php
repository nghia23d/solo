<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AdminModel extends Model
{
    protected $table = '';
    protected $folderUpload = '';
    public $timestamps = true;
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $searchField = [
        'id',
        'name',
    ];
    protected $fieldsBlock =[
        '_token',
        'thumb_current',
    ];

    public function deleteThumb($thumbName)
    {
        Storage::disk('admin_storage_image')->delete($this->folderUpload.'/'.$thumbName);
    }
    public function uploadThumb($thumb)
    {
        $thumbName = Str::random(10). '.' . $thumb->getClientOriginalExtension();
        $thumb->storeAs($this->folderUpload, $thumbName, 'admin_storage_image');
        return $thumbName;
    }
    public function deleteFieldsBlock($fields)
    {
        foreach ($this->fieldsBlock as $value) {
            unset($fields[$value]);
        }
        return $fields;
    }
}
