<?php

namespace App\Models;

use App\Models\AdminModel;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Article extends AdminModel
{
    protected $fillable = [
        'id',
        'name',
        'category_id',
        'content',
        'thumb',
        'status',
        'created_by',
        'modified_by'
    ];
    public function __construct()
    {
        $this->table        = 'article';
        $this->folderUpload = 'article';
        $this->searchField  = ['id','name','content'];
        $this->fieldsBlock  = ['_token','thumb_current'];
    }
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function listItems($params = null, $option = null)
    {
        $result = null;
        if ($option['task'] == "admin-list-items") {
            $query = self::with('category:id,name')->select('*')->orderBy('id', 'DESC');

            if ($params['filter']['status'] != 'all') {
                $query->where('status', $params['filter']['status']);
            }

            if ($params['search']['value'] !== '') {
                if ($params['search']['field'] == 'all') {
                    $query->where(function ($query) use ($params) {
                        foreach ($this->searchField as $field) {
                            $query->orWhere($field, 'LIKE', "%{$params['search']['value']}%");
                        }
                    });
                } elseif (in_array($params['search']['field'], $this->searchField)) {
                    $query->where($params['search']['field'], 'LIKE', "%{$params['search']['value']}%");
                };
            }

            $result = $query->paginate($params['pagination']['itemsPerPage']);
        }
        if ($option['task'] == "news-list-items-featured") {
            $query = self::select('id', 'name', 'content', 'thumb','category_id')
                            ->with('category:id,name')
                            ->where('status', 'active')
                            ->where('type', 'feature')
                            ->limit(3)
                            ->orderBy('id', 'DESC');
            $result = $query->get();
        };
        return $result;
    }

    public function countItems($params = null, $option = null)
    {
        $result = null;
        if ($option['task'] == "admin-count-items") {
            $query = $this::select('status', DB::raw('count(*) as count, status'));
            $result = $query->groupBy('status')
                            ->get()
                            ->toArray();
        }
        return $result;
    }
    public function saveItem($params = null, $option = null)
    {
        if ($option['task'] == "change-status-item") {
            $revertStatus = [
                'active'   => 'inactive',
                'inactive' => 'active'
            ];
            self::where('id', $params['id'])
                ->update(['status' => $revertStatus[$params['currentStatus']]]);
        }
        if ($option['task'] == "add-item") {
            $params['created_by'] = 'nghia le';
            $params['thumb']      = $this->uploadThumb($params['thumb']);
            self::insert($this->deleteFieldsBlock($params));
        }
        if ($option['task'] == "edit-item") {
            if (!empty($params['thumb'])) {
                $this->deleteThumb($params['thumb_current']);
                $params['thumb'] = $this->uploadThumb($params['thumb']);
            }
            $params['modified_by'] = 'nghia le';
            self::where('id', $params['id'])->update($this->deleteFieldsBlock($params));
        }
    }
    public function deleteItem($params = null, $option = null)
    {
        if ($option['task'] == "delete-item") {
            $item   = $this->getItem(['id' => $params['id']], ['task' => 'get-item']);
            $this->deleteThumb($item->thumb);
            $item->delete();
        }
    }
    public function getItem($params = null, $option = null)
    {
        $result = null;
        if ($option['task'] == "get-item") {
            $result  = self::find($params['id']);
        }
        return $result;
    }
}
