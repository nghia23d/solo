<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Category;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    private $table = 'article';

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id            = $this->id;

        $categoryModel = new Category();
        $itemsCategory = $categoryModel->listItems(null,['task' => 'admin-list-items-in-selectbox']);

        $condCategory  = 'bail|in:';
        $condThumb     = 'bail|required|image|max:1024';
        $condName      = "bail|required|between:3,50|unique:$this->table,name";

        foreach ($itemsCategory as $cid){
            $condCategory .= $cid->id .",";
        }
        $condCategory = rtrim($condCategory,",");

        //rules for edit
        if (!empty($id)) {
            $condThumb  = 'bail|image|max:1024';
            $condName  .=  ",$id";
        }
        return [
            'name'        => $condName,
            'content'     => 'bail|required|min:5',
            'category_id' => $condCategory,
            'status'      => 'bail|in:active,inactive',
            'thumb'       => $condThumb,
        ];
    }
    public function messages()
    {
        return [
            'name.required'    => 'Tên không được để trống',
            'name.between'     => 'Tên không được ít hơn :min  kí tực và nhỏ hơn :max',
            'content.required' => 'Content không được để trống',
            'status.in'        => 'Vui lòng chọn kích hoạt hoặc không kích hoạt',
            'category_id.in'   => 'Danh mục ko phù hợp'
        ];
    }
}
