<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    private $table = 'category';
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id        = $this->id;
        $condName  = "bail|required|between:3,50|unique:$this->table,name";
        //rules edit
        if (!empty($id)) {
            $condName  .=  ",$id";
        }
        return [
            'name'        => $condName,
            'status'      => 'bail|in:active,inactive',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'name.between'  => 'Tên không được ít hơn :min  kí tực và nhỏ hơn :max',
            'status.in'     => 'Vui lòng chọn kích hoạt hoặc không kích hoạt'
        ];
    }
}
