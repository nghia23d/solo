<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    private $table = 'slider';
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id        = $this->id;
        $condThumb = 'bail|required|image|max:1024';
        $condName  = "bail|required|between:3,50|unique:$this->table,name";
        //rules edit
        if (!empty($id)) {
            $condThumb  = 'bail|image|max:1024';
            $condName  .=  ",$id";
        }
        return [
            'name'        => $condName,
            'description' => 'bail|required|min:5',
            'link'        => 'bail|required|min:5|url',
            'status'      => 'bail|in:active,inactive',
            'thumb'       => $condThumb,
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'name.between' => 'Tên không được ít hơn :min  kí tực và nhỏ hơn :max',
            'description.required'  => 'Description không được để trống',
            'link.required'        => 'Link không được để trống',
            'link.min'        => 'Link không thể dưới :min ký tự',
            'link.url'        => 'Link ko đúng định dạng',
            'status.in'      => 'Vui lòng chọn kích hoạt hoặc không kích hoạt'
        ];
    }
}
