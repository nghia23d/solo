<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Category;
use App\Models\Article;

class HomeController extends Controller
{
    const PATH_VIEW      = 'user.page.home.';
    const PREFIX_MODULE  = 'Home';
    const ITEMS_PER_PAGE = 5;
    
    public function __construct()
    {
        $this->slider    = new Slider();
        $this->category  = new Category();
        $this->article   = new Article();

        $generalParams = [
            'prefixModule'  => self::PREFIX_MODULE,
            'listItemsMenu' => $this->category->listItems(null, ['task' => 'news-list-items'])
        ];
        view()->share('generalParams', $generalParams);
    }

    public function index(Request $request)
    {
        $itemsSlider   = $this->slider->listItems(null, ['task' => 'news-list-items']);
        $itemsCategory = $this->category->listItems(null, ['task' => 'news-list-items-is-home']);
        $itemsFeature  = $this->article->listItems(null,['task' => 'news-list-items-featured']);
        
        return view(self::PATH_VIEW . 'index', [
            'itemsFeature'    => $itemsFeature, 
            'itemsSlider'     => $itemsSlider,
            'itemsCategory'   => $itemsCategory,
        ]);
    }
}
