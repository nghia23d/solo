<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category as MainModel;
use App\Http\Requests\Admin\CategoryRequest as MainRequest ;

class CategoryController extends Controller
{
    const PATH_VIEW      = 'admin.page.category.';
    const PREFIX_MODULE  = 'category';
    const ITEMS_PER_PAGE = 10;
    
    public function __construct()
    {
        $this->model  = new MainModel();
        $this->params['pagination']['itemsPerPage'] = self::ITEMS_PER_PAGE;
        view()->share('prefixModule', self::PREFIX_MODULE);
    }

    public function index(Request $request)
    {
        $this->params['filter']['status'] = $request->input('filter_status', 'all');
        $this->params['search']['field']  = $request->input('search_field', '');
        $this->params['search']['value']  = $request->input('search_value', '');
        $items = $this->model->listItems($this->params, ['task' => "admin-list-items"]);
        $countByStatus = $this->model->countItems($this->params, ['task' => "admin-count-items"]);
        return view(self::PATH_VIEW . 'index', [
            'items'         => $items,
            'countByStatus' => $countByStatus,
            'params'        => $this->params,
        ]);
    }

    public function save(MainRequest $request)
    {
        //add
        $task   = 'add-item';
        $notify = 'Add item successful';
        //edit
        if ($request->id !== null) {
            $task   = 'edit-item';
            $notify = 'Edit item successful';
        }
        $this->model->saveItem($request->all(), ['task' => $task]);
        return redirect()->route(self::PREFIX_MODULE)->with('success', $notify);
    }

    public function delete(Request $request)
    {
        $params['id'] = $request->id;
        $items = $this->model->deleteItem($params, ['task' => "delete-item"]);
        return redirect()->route(self::PREFIX_MODULE)->with('success', 'Xóa thành công');
    }
    public function status(Request $request)
    {
        $params['currentStatus'] = $request->status;
        $params['id']            = $request->id;
        $this->model->saveItem($params, ['task' => "change-status-item"]);
        return redirect()->route(self::PREFIX_MODULE)->with('success', 'Cập nhật trạng thái thành công');
    }
    public function changeIsHome(Request $request)
    {
        $params['currentIsHome'] = $request->isHome;
        $params['id']            = $request->id;
        $this->model->saveItem($params, ['task' => "change-is-home"]);
        return redirect()->route(self::PREFIX_MODULE)->with('success', 'Cập nhật trạng thái Is Home thành công');
    }
}
