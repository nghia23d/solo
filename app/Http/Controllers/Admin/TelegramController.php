<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Helpers\Telegram as Telegram;

// use App\Models\Category as MainModel;
// use App\Http\Requests\Admin\TelegramRequest as MainRequest ;

class TelegramController extends Controller
{
    const PATH_VIEW      = 'admin.page.telegram.';
    const PREFIX_MODULE  = 'telegram';
    const ITEMS_PER_PAGE = 5;
    
    public function __construct()
    {
        // $this->model  = new MainModel();
        // $this->params['pagination']['itemsPerPage'] = self::ITEMS_PER_PAGE;
        // view()->share('prefixModule', self::PREFIX_MODULE);
    }

    public function index(Request $request)
    {
        return view(self::PATH_VIEW . 'index', [
           'prefixModule' => self::PREFIX_MODULE,
        ]);
    }
    public function save($value)
    {
        $item = DB::table('telegram')->insert([
            'first_name' => $value->chat->first_name,
            'last_name'  => $value->chat->last_name,
            'chat_id'    => $value->chat->id,
            'username'   => (isset($value->chat->username)) ? $value->chat->username : '',
        ]);
        return $item;
    }
    public function connectBotTelegram(Request $request)
    {
        $result = false;
        $url  = 'https://api.telegram.org/bot1002696012:AAHAUj92CgF343HzPorpEgQ5RS0j1H7YY4Q/getupdates';
        $data = json_decode(@file_get_contents($url));
        if (count($data->result) == 100) {
            $update_id = end($data->result)->update_id;
            @file_get_contents($url.'?offset='.$update_id);
            return $this->connectBotTelegram($request);
        }
        foreach ($data->result as $value) {
            if ($value->message->text  == $request->token) {
                $result = $this->save($value->message);
            }
        };
        if ($result) {
            Telegram::sendMessage($value->chat->id, 'Xin chúc mừng bạn đã kết nối với BOT thành công');
            return back()->with('success', 'thêm thành công');
        } else {
            return back()->with('error', 'them that bai');
        }
    }
}
