<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    const PATH_VIEW    = 'admin.dashboard.';
    const PREFIX_MODULE = 'dashboard';
    
    public function __construct()
    {
        view()->share('prefixModule', self::PREFIX_MODULE);
    }

    public function index()
    {
        return view(self::PATH_VIEW . 'index');
    }
}
