<?php
    namespace App\Helpers;

    use Config;
    use Carbon\Carbon;

    class Telegram
    {
        public static function sendMessage($chat_id , $message = '')
        {
            $chat_id = (!empty($chat_id)) ? $chat_id : config('zvn.telegram.chat_id');
            $bot_token = config('zvn.telegram.bot_token');
            $url = 'https://api.telegram.org/bot' . $bot_token . '/sendMessage?chat_id=' . $chat_id . '&parse_mode=HTML&text=' . urlencode($message);
            @file_get_contents($url);
        }
    }
