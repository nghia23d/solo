<?php
    namespace App\Helpers;

    use Config;
    use Carbon\Carbon;

    class Template
    {
        public static function showInfoHistory($by, $time)
        {
            $time = is_null($time) ? null : Carbon::parse($time)->format(config('zvn.format_time.long'));
            $xhtml = "<p><i class='fa fa-user'></i> $by</p>
                      <p><i class='fa fa-clock-o'></i> ".$time."</p>";
            return $xhtml;
        }
        
        public static function showInfoStatus($prefixModule, $id, $status)
        {
            $templateStatus = config('zvn.template.status');
            try {
                $currentStatus = $templateStatus[$status];
            } catch (\Exception $e) {
                $currentStatus = [
                    'name'  => 'Chưa xác định',
                    'class' => '',
                ];
            }
            $xhtml = '<a href="'. route($prefixModule.'/change-status', ['status' => $status, 'id' => $id]).'" type="button" class="btn btn-round '.$currentStatus['class'].'">'.$currentStatus['name'].'</a>';
            return $xhtml;
        }
        public static function showInfoIsHome($prefixModule, $id, $isHome)
        {
            $templateStatus = config('zvn.template.is_home');
            try {
                $currentStatus = $templateStatus[$isHome];
            } catch (\Exception $e) {
                $currentStatus = [
                    'name'  => 'Chưa xác định',
                    'class' => '',
                ];
            }
            $xhtml = '<a href="'. route($prefixModule.'/change-is-home', ['isHome' => $isHome, 'id' => $id]).'" type="button" class="btn btn-round '.$currentStatus['class'].'">'.$currentStatus['name'].'</a>';
            return $xhtml;
        }

        public static function showButtonAction($prefixModule, $id)
        {
            $infoButton   = config('zvn.template.button');
            $buttonArea   = config('zvn.config.button_area');
            $prefixModule = array_key_exists($prefixModule, $buttonArea) ? $prefixModule : 'default';
            $listButton   = $buttonArea[$prefixModule];
            $xhtml = '';
            foreach ($listButton as $btn) {
                $link = ($infoButton[$btn]['link'] !== '#') ? route($prefixModule.$infoButton[$btn]['link'], ['id' => $id]) : '#';
                $xhtml .= '<a href="'.$link.'" data-id='.$id.'  type="button" class="btn btn-icon  '.$infoButton[$btn]['class'].'" data-toggle="tooltip" data-placement="top" data-original-title="'.$infoButton[$btn]['title'].'">
                               <i class="fa '.$infoButton[$btn]['icon'].'"></i> 
                            </a>';
            };
            return $xhtml;
        }
        public static function showButtonFilter($countByStatus, $currentFilter, $paramsCurrentSearch)
        {
            $linkCurrentSearch = ($paramsCurrentSearch['value'] !== '') ? '&search_field='.$paramsCurrentSearch['field'].'&search_value='.$paramsCurrentSearch['value'] : null;
            $templateStatus = config('zvn.template.status');
            $xhtml = '';
            if (count($countByStatus) > 0) {
                array_unshift($countByStatus, [
                    'count' => array_sum(array_column($countByStatus, 'count')),
                    'status' => 'all',
                ]);
                foreach ($countByStatus as $value) {
                    try {
                        //get info of button in config
                        $name = $templateStatus[$value['status']];
                    } catch (\Exception $e) {
                        $name['name'] = 'Chưa xác định';
                    }
                    $classActive = ($value['status'] == $currentFilter) ? 'btn-danger' : 'btn-primary';
                    $xhtml .= '<a href="?filter_status='.$value['status'].$linkCurrentSearch.'" class="btn '.$classActive.'">
                                    '.$name['name'].' <span class="badge bg-white"> '.$value['count'].'</span>
                                </a>';
                }
            }
            return $xhtml;
        }
        public static function showAreaSearch($prefixModule, $paramsSearch)
        {
            $xhtml = '';
            $xhtmlField = '';
            $templateField = config('zvn.template.search');
            $fieldInModule = config('zvn.config.search');
            $prefixModule = array_key_exists($prefixModule, $fieldInModule) ? $prefixModule : 'default';

            $searchField = (in_array($paramsSearch['field'], $fieldInModule[$prefixModule])) ? $paramsSearch['field'] : 'all';
            
            foreach ($fieldInModule[$prefixModule] as $field) {
                $xhtmlField .= '<li><a href="#" class="select-field" data-field="'.$field.'">'.$templateField[$field]['name'].'</a></li>';
            };
            $xhtml = '<div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle btn-active-field" data-toggle="dropdown" aria-expanded="false">
                               '.$templateField[$searchField]['name'].' <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                               '.$xhtmlField.'
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="search_value" value="'.$paramsSearch['value'].'">
                        <input type="hidden" class="form-control" name="search_field" value="'.$searchField.'">
                        <span class="input-group-btn">
                            <button id="btn-clear-search" type="button" class="btn btn-success" >Xóa tìm kiếm</button>
                            <button id="btn-search" type="button" class="btn btn-primary">Tìm kiếm</button>
                        </span>
                    </div>';
            return $xhtml;
        }
    }
