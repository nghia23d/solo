<?php

return [
    'url' => [
        'prefix_admin' => 'admin',
        'prefix_news'  => 'news',
    ],
    'telegram' => [
        'chat_id'    => '',
        'bot_token'  => '1002696012:AAHAUj92CgF343HzPorpEgQ5RS0j1H7YY4Q',
    ],
    
    'format_time' => [
        'long'  => 'Y-m-d H:i:s',
        'short' => 'Y-m-d'
    ],

    'template' => [
        'status' => [
            'all'      => ['name' => 'Tất cả', 'class' => ''],
            'active'   => ['name' => 'Đang kích hoạt', 'class' => 'btn-success'],
            'inactive' => ['name' => 'Chưa kích hoạt', 'class' => 'btn-danger'],
        ],
        'is_home' => [
            ['name' => 'Không hiển thị', 'class' => 'btn-warning'],
            ['name' => 'Hiển thị', 'class' => 'btn-success'],
        ],
        'search' => [
            'all'         => ['name' => 'Search by All'],
            'id'          => ['name' => 'Search by ID'],
            'name'        => ['name' => 'Search by Name'],
            'username'    => ['name' => 'Search by Username'],
            'fullname'    => ['name' => 'Search by Fullname'],
            'email'       => ['name' => 'Search by Email'],
            'description' => ['name' => 'Search by Description'],
            'link'        => ['name' => 'Search by Link'],
            'content'     => ['name' => 'Search by Content'],
        ],
        'button' => [
            'edit'   => ['class' => 'btn-success btn-edit' ,'icon' => 'fa-pencil', 'title' => 'Edit', 'link' => '#'],
            'delete' => ['class' => 'btn-danger' ,'icon' => 'fa-trash', 'title' => 'Delete', 'link' => '/delete'],
        ],
    ],
    'config' => [
        'button_area' => [
            'default'   => ['edit', 'delete'],
            'slider'    => ['edit', 'delete'],
            'article'   => ['edit', 'delete'],
            'category'  => ['edit', 'delete'],
        ],
        'search' => [
            'default'   => ['all', 'id'],
            'slider'    => ['all', 'id', 'name', 'description', 'link'],
            'category'  => ['all', 'id', 'name',],
            'article'   => ['all', 'id', 'name','content']
        ],
    ]
];
