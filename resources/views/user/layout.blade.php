{{-- {{asset('admin/asset/iCheck/skins/flat/green.css')}} --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Blog | Index</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Tech Mag template project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('user/news/images//favicons.png')}}" rel="icon" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="{{asset('user/news/css/bootstrap-4.1.2/bootstrap.min.css')}}">
    <link href="{{asset('user/news/css/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('user/news/js/OwlCarousel2-2.2.1/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/news/js/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/news/js/OwlCarousel2-2.2.1/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/news/css/main_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/news/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/news/css/my-style.css')}}">
</head>
<body>
<div class="super_container">
    <!-- Header -->
   @include('user.block.header')
    <!-- Menu -->
    @yield('content')
    <!-- Footer -->
    @include('user.block.footer')
   
</div>
<script src="{{asset('user/news/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('user/news/css/bootstrap-4.1.2/popper.js')}}"></script>
<script src="{{asset('user/news/css/bootstrap-4.1.2/bootstrap.min.js')}}"></script>
<script src="{{asset('user/news/js/greensock/TweenMax.min.js')}}"></script>
<script src="{{asset('user/news/js/greensock/TimelineMax.min.js')}}"></script>
<script src="{{asset('user/news/js/scrollmagic/ScrollMagic.min.js')}}"></script>
<script src="{{asset('user/news/js/greensock/animation.gsap.min.js')}}"></script>
<script src="{{asset('user/news/js/greensock/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('user/news/js/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{asset('user/news/js/easing/easing.js')}}"></script>
<script src="{{asset('user/news/js/parallax-js-master/parallax.min.js')}}"></script>
<script src="{{asset('user/news/js/custom.js')}}"></script>
</body>
</html>