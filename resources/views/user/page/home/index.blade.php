@extends('user.layout')
@section('content')
    <!-- Home -->
    @include('user.block.slider')
    <!-- Content Container -->
    <div class="content_container">
        <div class="container">
            <div class="row">
                <!-- Main Content -->
                <div class="col-lg-9">
                    <div class="main_content">
                        <!-- Featured -->
                        @include('user.block.featured',['itemsFeature' => $itemsFeature])
                        <!-- Category -->
                        @include('user.page.home.child-index.category',['itemsCategory'=> $itemsCategory])
                    </div>
                </div>
                <!-- Sidebar -->
                <div class="col-lg-3">
                    <div class="sidebar">
                        <!-- Latest Posts -->
                        @include('user.block.recent')
                        <!-- Advertisement -->
                        <!-- Extra -->
                        @include('user.block.ads')
                        <!-- Most Viewed -->
                        @include('user.block.most_view')
                        <!-- Tags -->
                        @include('user.block.tags')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection