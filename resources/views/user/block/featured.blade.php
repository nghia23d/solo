@php
use App\Helpers\Template as Template;
$value = $itemsFeature->first();
$itemsFeatureLeft  = $itemsFeature->forget(0);
@endphp
<div class="featured">
    <div class="featured_title">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section_title_container d-flex flex-row align-items-start justify-content-start">
                        <div>
                            <div class="section_title">Nổi bậc</div>
                        </div>
                        <div class="section_bar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Featured Title -->
    <div class="row">
        <div class="col-lg-8">
            <!-- Post -->
            <div class="post_item post_v_large d-flex flex-column align-items-start justify-content-start">
                <div class="post_item post_v_small d-flex flex-column align-items-start justify-content-start">
                <div class="post_image"><img src="{{asset('user/news/images//article/'.$value->thumb)}}"
                                                alt="{{asset('user/news/images//article/'.$value->thumb)}}"
                                                class="img-fluid w-100"></div>
                    <div class="post_content">
                        <div class="post_category cat_technology ">
                            <a href="the-loai/the-thao-1.html">{{$value->category->name}}</a>
                        </div>
                        <div class="post_title"><a
                                href="bai-viet/liverpool-chi-duoc-nang-cup-phien-ban-neu-vo-dich-hom-nay-4.html">{{$value->name}}</a></div>
                        <div class="post_info d-flex flex-row align-items-center justify-content-start">
                            <div class="post_author d-flex flex-row align-items-center justify-content-start">
                                <div class="post_author_name"><a href="#">{{ $value->created_by }}</a>
                                </div>
                            </div>
                            <div class="post_date"><a href="#">{{ $value->created }}</a></div>
                        </div>
                        <div class="post_text">
                            <p>">{!! $value->content !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            @foreach ($itemsFeatureLeft  as $value)
            <div>
                <div class="post_item post_v_small d-flex flex-column align-items-start justify-content-start">
                    <div class="post_image"><img src="{{asset('user/news/images//article/'.$value->thumb)}}"
                                                alt="{{asset('user/news/images//article/'.$value->thumb)}}"
                                                class="img-fluid w-100"></div>
                    <div class="post_content">
                        <div class="post_category cat_technology ">
                            <a href="the-loai/the-thao-1.html">{{$value->category->name}}</a>
                        </div>
                        <div class="post_title"><a
                                href="bai-viet/bottas-gianh-pole-chang-thu-ba-lien-tiep-5.html">{{$value->name}}</a></div>
                        <div class="post_info d-flex flex-row align-items-center justify-content-start">
                            <div class="post_author d-flex flex-row align-items-center justify-content-start">
                                <div class="post_author_name"><a href="#">{{ $value->created_by }}</a>
                                </div>
                            </div>
                            <div class="post_date"><a href="#">{{ $value->created }}</a></div>
                        </div>
                        <div class="post_text">
                            <p>{!! $value->content !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>