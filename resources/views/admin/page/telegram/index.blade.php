@php
use App\Helpers\Template as Template;
@endphp
@section('title','category')
@section('content')
            @include('admin.general.page_header')
            <div class="row">
              
            </div>
            @include('admin.general.error')
            <!-- modal form -->
            @include('admin.page.telegram.form')
@endsection
@section('js')
<script>
    var token = $("#token")
    token.val(parseInt(Math.random()*18546876546));

    $(".btn-add").click(function() {
        $("#formModal").modal({backdrop: 'static', keyboard: false});
            
    });
</script>
@endsection
@extends('admin.layout')


