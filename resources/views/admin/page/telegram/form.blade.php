<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thêm mới {{$prefixModule}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{route($prefixModule.'/connectBotTelegram')}}" accept-charset="UTF-8" enctype="multipart/form-data" class="form-horizontal form-label-left" id="main-form">
            {{ csrf_field() }}
            <div class="form-group">
              <h3 class="text-danger">Để kết nối bot telegram  vào link dưới vào copy đoạn token vào BOT và nhẫn kiểm tra</h3>
              <label for="is_home" class="control-label col-md-3 col-sm-3 col-xs-12">Token</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="token" id="token" readonly required class="form-control">
                  <button type="sumit" class="btn btn-primary">Xác nhận</button>
              </div>
          </div>
            <div class="ln_solid"></div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>