<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thêm mới {{$prefixModule}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{route($prefixModule.'/save')}}" accept-charset="UTF-8" enctype="multipart/form-data" class="form-horizontal form-label-left" id="main-form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control col-md-6 col-xs-12" name="name" type="text"  id="name">
                </div>
            </div>
            <div class="form-group">
                <label for="status" class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control col-md-6 col-xs-12" id="status" name="status">
                        <option value="active" >Kích hoạt</option>
                        <option value="inactive">Chưa kích hoạt</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
              <label for="is_home" class="control-label col-md-3 col-sm-3 col-xs-12">Hiển thị trang chủ</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control col-md-6 col-xs-12" id="is_home" name="is_home">
                      <option value="1" >Yes</option>
                      <option value="0">No</option>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label for="display" class="control-label col-md-3 col-sm-3 col-xs-12">Kiểu hiển thị</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control col-md-6 col-xs-12" id="display" name="display">
                      <option value="list" >Danh sách</option>
                      <option value="grid">Lưới</option>
                  </select>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <input name="id" class="id-current" type="hidden">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>