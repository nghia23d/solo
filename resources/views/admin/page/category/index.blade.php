@php
use App\Helpers\Template as Template;
@endphp
@section('title','category')
@section('content')
            @include('admin.general.page_header')
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bộ lọc</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! Template::showButtonFilter($countByStatus,$params['filter']['status'],$params['search']) !!}
                                </div>
                            <div class="col-md-6">{!! Template::showAreaSearch($prefixModule,$params['search']) !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.general.error')
            <!--box-lists-->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Danh sách</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action table-responsive">
                                    <thead>
                                    <tr class="headings">
                                        <th class="column-title">#</th>
                                        <th class="column-title">Name</th>
                                        <th class="column-title">Tổng số bài viết</th>
                                        <th class="column-title">Trạng thái</th>
                                        <th class="column-title">Trạng thái IS HOME</th>
                                        <th class="column-title">Thông tin tạo</th>
                                        <th class="column-title">Thông tin sửa</th>
                                        <th class="column-title">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($items as $key => $value)
                                    <tr id="row-{{$value->id}}" class="@if(($key + 1) %2 == 0) even @else odd @endif pointer">
                                        <td >{{$key + 1}}</td>
                                            <td style="width: 40%">
                                                <p><b>Name: <span class="name-category"> {{$value->name}} </span></b></p>
                                                <p class="hidden status-category">{{$value->status}}</p>
                                                <p class="hidden is-home-category">{{$value->is_home}}</p>
                                                <p class="hidden display-type">{{$value->display}}</p>
                                            </td>
                                        <td> {{$value->articles_count}}</td>
                                            <td> {!! Template::showInfoStatus($prefixModule,$value->id,$value->status) !!}</td>
                                            <td> {!! Template::showInfoIsHome($prefixModule,$value->id,$value->is_home) !!}</td>
                                            <td> {!! Template::showInfoHistory($value->created_by,$value->created) !!} </td>
                                            <td> {!! Template::showInfoHistory($value->modified_by,$value->modified) !!} </td>
                                            <td class="last">
                                                <div class="zvn-box-btn-filter">
                                                {!! Template::showButtonAction($prefixModule,$value->id) !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @if(count($items) == 0)
                                        @include('admin.general.empty_data',['colspan' => 6])
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-box-lists-->
            <!--box-pagination-->
            @if(count($items) > 0)
                @include('admin.general.pagination')
            @endif
            <!--end-box-pagination-->
            <!-- modal form -->
            @include('admin.page.category.form')
@endsection
@section('js')
<script>
        let searchField = $('input[name = search_field]');

        $('.select-field').click(function(e){
            e.preventDefault();
            $('.btn-active-field').html($(this).html() + ' <span class="caret"></span>');
            searchField.val($(this).data("field"));
        });
        
        $('#btn-search').click(function(){      
            let searchValue    = $('input[name = search_value]');
            let filterStatus   = (getUrlParameter('filter_status')) ? getUrlParameter('filter_status') : 'all' ;

            if(searchValue.val().replace(/\s/g,"") == "")
                toastr.error('Hãy nhập từ khóa phù hợp');
            else
                window.location.href = window.location.pathname +
                                    '?filter_status=' + filterStatus +
                                    '&search_field='  + searchField.val()  +
                                    '&search_value='  + searchValue.val();
        });
    
        function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };
        $('.btn-edit').click(function() {
            $("#formModal").modal({backdrop: 'static', keyboard: false});
            let id              = $(this).data('id');
            let rowParent       = $('tr#row-'+id);	
            let name            = rowParent.find(".name-category");
            let status          = rowParent.find(".status-category");
            let isHome          = rowParent.find(".is-home-category");
            let display         = rowParent.find(".display-type");
            
            //set value modal
            $('.id-current').val(id);
            $('input#name').val(name.html());
            $("#status").val(status.html());
            $("#is_home").val(isHome.html());
            $("#display").val(display.html());
        });

        $(".btn-add").click(function() {
            $("#formModal").modal({backdrop: 'static', keyboard: false});
            $('#main-form').find("input, textarea").not("[name='_token']").val("");
            $("select").each(function() {
                $(this).val($(this).find('option:first').val());
            });  
        });
</script>
@endsection
@extends('admin.layout')


