<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thêm mới {{$prefixModule}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{route($prefixModule.'/save')}}" accept-charset="UTF-8" enctype="multipart/form-data" class="form-horizontal form-label-left" id="main-form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control col-md-6 col-xs-12" name="name" type="text"  id="name">
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control col-md-6 col-xs-12" name="description" type="text"  id="description">
                </div>
            </div>
            <div class="form-group">
                <label for="status" class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control col-md-6 col-xs-12" id="status" name="status">
                        <option value="active" >Kích hoạt</option>
                        <option value="inactive">Chưa kích hoạt</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="link" class="control-label col-md-3 col-sm-3 col-xs-12">Link</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control col-md-6 col-xs-12" name="link" type="text"  id="link">
                </div>
            </div>
            <div class="form-group">
                <label for="thumb" class="control-label col-md-3 col-sm-3 col-xs-12">Thumb</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control col-md-6 col-xs-12" name="thumb" type="file" id="thumb" > 
                    <p style="margin-top: 50px;"><img src="#"  class="zvn-thumb img-thumb"></p>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <input name="id" class="id-current" type="hidden">
                    <input name="thumb_current" class="thumb-current" type="hidden" >
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>