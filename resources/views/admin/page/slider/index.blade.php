@php
use App\Helpers\Template as Template;
@endphp
@section('title','Slider')
@section('content')
            @include('admin.general.page_header')
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Bộ lọc</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! Template::showButtonFilter($countByStatus,$params['filter']['status'],$params['search']) !!}
                                </div>
                            <div class="col-md-6">{!! Template::showAreaSearch($prefixModule,$params['search']) !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.general.error')
            <!--box-lists-->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Danh sách</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action table-responsive">
                                    <thead>
                                    <tr class="headings">
                                        <th class="column-title">#</th>
                                        <th class="column-title">Slider Info</th>
                                        <th class="column-title">Trạng thái</th>
                                        <th class="column-title">Thông tin tạo</th>
                                        <th class="column-title">Thông tin sửa</th>
                                        <th class="column-title">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($items as $key => $value)
                                    <tr id="row-{{$value->id}}" class="@if(($key + 1) %2 == 0) even @else odd @endif pointer">
                                        <td >{{$key + 1}}</td>
                                            <td style="width: 40%">
                                                <p><b>Name: <span class="name-slider"> {{$value->name}} </span></b></p>
                                                <p><b>Description:  <span class="description-slider"> {{$value->description}}</span> </b></p>
                                                <p><b>Link: <a class="link-slider" href="{{$value->link}}">{{$value->link}}</a> </b></p>
                                                <p class="hidden status-slider">{{$value->status}}</p>
                                            <div> <img data-link-thumb='{{$value->thumb}}' src="{{asset('user/news/images//'.$prefixModule.'/'.$value->thumb)}}" class="zvn-thumb thumb-slider" alt="{{$value->name}}"> </div>
                                            </td>
                                            <td> {!! Template::showInfoStatus($prefixModule,$value->id,$value->status) !!}</td>
                                            <td> {!! Template::showInfoHistory($value->created_by,$value->created) !!} </td>
                                            <td> {!! Template::showInfoHistory($value->modified_by,$value->modified) !!} </td>
                                            <td class="last">
                                                <div class="zvn-box-btn-filter">
                                                {!! Template::showButtonAction($prefixModule,$value->id) !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @if(count($items) == 0)
                                        @include('admin.general.empty_data',['colspan' => 6])
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-box-lists-->
            <!--box-pagination-->
            @if(count($items) > 0)
                @include('admin.general.pagination')
            @endif
            <!--end-box-pagination-->
            <!-- modal form -->
            @include('admin.page.slider.form')
@endsection
@section('js')
<script>
        let searchField = $('input[name = search_field]');

        $('.select-field').click(function(e){
            e.preventDefault();
            $('.btn-active-field').html($(this).html() + ' <span class="caret"></span>');
            searchField.val($(this).data("field"));
        });
        
        $('#btn-search').click(function(){      
            let searchValue    = $('input[name = search_value]');
            let filterStatus   = (getUrlParameter('filter_status')) ? getUrlParameter('filter_status') : 'all' ;

            if(searchValue.val().replace(/\s/g,"") == "")
                toastr.error('Hãy nhập từ khóa phù hợp');
            else
                window.location.href = window.location.pathname +
                                    '?filter_status=' + filterStatus +
                                    '&search_field='  + searchField.val()  +
                                    '&search_value='  + searchValue.val();
        });
    
        function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };
        $('.btn-edit').click(function() {
            $("#formModal").modal({backdrop: 'static', keyboard: false});
            let id              = $(this).data('id');
            let rowParent       = $('tr#row-'+id);	
            let name            = rowParent.find(".name-slider");
            let description     = rowParent.find(".description-slider");
            let status          = rowParent.find(".status-slider");
            let link            = rowParent.find(".link-slider");
            let thumb           = rowParent.find(".thumb-slider");
            
            //set value modal
            $('.id-current').val(id);
            $('input#name').val(name.html());
            $('input#description').val(description.html());
            $("#status").val(status.html());  
            $('input#link').val(link.html());
            $('.thumb-current').val(thumb.data('link-thumb'));
            $('.img-thumb').attr("src", thumb.attr('src')).removeClass('hidden');
        });

        $(".btn-add").click(function() {
            $("#formModal").modal({backdrop: 'static', keyboard: false});
            $('#main-form').find("input, textarea").not("[name='_token']").val("");
            $("select").each(function() {
                $(this).val($(this).find('option:first').val());
            });
            $('.img-thumb').addClass('hidden');
        });
</script>
@endsection
@extends('admin.layout')


