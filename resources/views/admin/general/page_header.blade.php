<div class="page-header zvn-page-header clearfix">
    <div class="zvn-page-header-title">
    <h3>Quản lý {{$prefixModule}}</h3>
    </div>
    <div class="zvn-add-new pull-right">
        <a class="btn btn-primary btn-add" data-toggle="modal" data-target="#exampleModal" class="btn btn-success"><i
                class="fa fa-plus-circle"></i> Thêm mới</a>
    </div>
</div>