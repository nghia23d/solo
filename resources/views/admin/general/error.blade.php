@if ($errors->any())
<div class="alert alert-danger">
    <h4> <span class="fa fa-warning"></span> Có lỗi xảy ra</h4>
        @foreach ($errors->all() as $error)
            <p>- {{ $error }}</p>
        @endforeach
</div>
@endif