<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Phân trang
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-6">
                        <p class="m-b-0">Tổng số phần tử : <b>{{$items->total()}}</b></p>
                    <p class="m-b-0">Tổng số phần tử trên trang: <b>{{$items->perPage()}}</b></p>
                        <p class="m-b-0">Tổng số trang: <b>{{$items->lastPage()}}</b></p>
                    </div>
                    <div class="col-md-6 text-right">
                        {{ $items->appends(request()->input())->links('admin.general.pagination_view') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>