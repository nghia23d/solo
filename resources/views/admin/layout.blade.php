<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="img/favicon.ico" type="image/ico"/>
    <title>Admin |  @yield('title')</title>
    <!-- Bootstrap -->
    <link href="{{asset('admin/asset/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('admin/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('admin/asset/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('admin/asset/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{asset('admin/asset/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet')}}">
    <!-- Custom Theme Style -->
    <link href="{{asset('admin/css/custom.min.css')}}" rel="stylesheet">
    <!-- CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!-- Custom Theme Style -->
    <link href="{{asset('admin/css/mycss.css')}}" rel="stylesheet">
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        @include('admin.block.sidebar')
        @include('admin.block.menu_top')

        <div class="right_col" role="main">
            <div>
                @yield('content')
            </div>
        </div>
        
        @include('admin.block.footer')
    
    </div>
</div>
<!-- jQuery -->
<script src="{{asset('admin/js/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('admin/asset/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('admin/js/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{asset('admin/asset/nprogress/nprogress.js')}}"></script>
<!-- bootstrap-progressbar -->
<script src="{{asset('admin/asset/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('admin/asset/iCheck/icheck.min.js')}}"></script>
<!-- ckedtiro -->
<script src="{{asset('admin/js/ckeditor/ckeditor.js')}}"></script>
<!-- CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!-- Custom Theme Scripts -->
<script src="{{asset('admin/js/custom.min.js')}}"></script>
    
<script>
    $(document).ready(function() {
        @if (session('info'))
            toastr.info("{{session('info')}}")
        @elseif (session('success'))
            toastr.success("{{session('success')}}")
        @elseif (session('error'))
            toastr.error("{{session('error')}}")
        @endif
    });
</script>
@yield('js')
</body>
</html>