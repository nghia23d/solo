<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
$prefixAdmin = config('zvn.url.prefix_admin');
$prefixNews  = config('zvn.url.prefix_news');

Route::get('/', function () {
    return redirect('news');
});

Route::prefix($prefixAdmin)->group(function () {
    // ==================================== DASHBOARD ================================
    $prefix = 'dashboard';
    $controller = 'Admin\DashboardController@';
    Route::prefix($prefix)->group(function () use ($prefix,$controller) {
        Route::get('/', $controller . 'index')->name($prefix);
    });
    // ==================================== SLIDER ================================
    $prefix = 'slider';
    $controller = 'Admin\SliderController@';
    Route::prefix($prefix)->group(function () use ($prefix,$controller) {
        Route::get('/', $controller . 'index')->name($prefix);
        Route::post('save', $controller . 'save')->name($prefix.'/save');
        Route::get('/delete/{id}', $controller . 'delete')->where('id', '[0-9]+')->name($prefix.'/delete');
        Route::get('/change-status-{status}/{id}', $controller . 'status')->where('id', '[0-9]+')->name($prefix.'/change-status');
    });
    // ==================================== CATEGORY ================================
    $prefix = 'category';
    $controller = 'Admin\CategoryController@';
    Route::prefix($prefix)->group(function () use ($prefix,$controller) {
        Route::get('/', $controller . 'index')->name($prefix);
        Route::post('save', $controller . 'save')->name($prefix.'/save');
        Route::get('/delete/{id}', $controller . 'delete')->where('id', '[0-9]+')->name($prefix.'/delete');
        Route::get('/change-status-{status}/{id}', $controller . 'status')->where('id', '[0-9]+')->name($prefix.'/change-status');
        Route::get('/change-is-home-{isHome}/{id}', $controller . 'changeIsHome')->where(['id' => '[0-9]+','ishome' => '[0-1]'])->name($prefix.'/change-is-home');
    });
    // ==================================== ARTICLE ================================
    $prefix = 'article';
    $controller = 'Admin\ArticleController@';
    Route::prefix($prefix)->group(function () use ($prefix,$controller) {
        Route::get('/', $controller . 'index')->name($prefix);
        Route::post('save', $controller . 'save')->name($prefix.'/save');
        Route::get('/delete/{id}', $controller . 'delete')->where('id', '[0-9]+')->name($prefix.'/delete');
        Route::get('/change-status-{status}/{id}', $controller . 'status')->where('id', '[0-9]+')->name($prefix.'/change-status');
    });
    // ==================================== BOT TELEGRAM ================================
    $prefix = 'telegram';
    $controller = 'Admin\TelegramController@';
    Route::prefix($prefix)->group(function () use ($prefix,$controller) {
        Route::get('/', $controller . 'index')->name($prefix);
        Route::post('save', $controller . 'connectBotTelegram')->name($prefix.'/connectBotTelegram');
    });
});

Route::prefix($prefixNews)->group(function () {
    // ==================================== HOMPAGE ================================
    $prefix = '';
    $controller = 'User\HomeController@';
    Route::prefix($prefix)->group(function () use ($prefix,$controller) {
        Route::get('/', $controller . 'index')->name('home');
    });
    // ==================================== SLIDER ================================
    // $prefix = 'slider';
    // $controller = 'Admin\SliderController@';
    // Route::prefix($prefix)->group(function () use ($prefix,$controller) {
    //     Route::get('/', $controller . 'index')->name($prefix);
    //     Route::post('save', $controller . 'save')->name($prefix.'/save');
    //     Route::get('/delete/{id}', $controller . 'delete')->where('id', '[0-9]+')->name($prefix.'/delete');
    //     Route::get('/change-status-{status}/{id}', $controller . 'status')->where('id', '[0-9]+')->name($prefix.'/change-status');
    // });
});
